const currentScript = document.currentScript;
const lang = currentScript.getAttribute("lang");
const landingId = currentScript.getAttribute("lid");

async function loadLanguages() {
    const response = await fetch(
        window.location.origin + `/lander/postcomments1/languages.json`
    );
    const data = await response.json();
    return data;
}

async function main() {
    const languages = await loadLanguages();

    const container = document.createElement("div");
    container.classList.add("fake-comment-poster");

    container.style.backgroundColor = "#e5e5e5";
    container.style.width = "100%";
    container.style.padding = "20px";
    container.style.margin = "0 auto";
    container.style.boxSizing = "border-box";

    const inputName = document.createElement("input");
    inputName.required = true;
    inputName.classList.add("fake-comment-poster-name");
    inputName.type = "text";
    inputName.placeholder = languages[lang].name;
    inputName.style.width = "100%";
    inputName.style.maxWidth = "100%";
    inputName.style.padding = "10px";
    inputName.style.boxSizing = "border-box";

    const textareaComment = document.createElement("textarea");
    textareaComment.required = true;
    textareaComment.classList.add("fake-comment-poster-text");
    textareaComment.placeholder = languages[lang].text;
    textareaComment.style.padding = "10px";
    textareaComment.style.display = "block";
    textareaComment.style.marginTop = "10px";
    textareaComment.style.width = "100%";
    textareaComment.style.maxWidth = "100%";
    textareaComment.style.height = "20vw";
    textareaComment.style.maxHeight = "80px";
    textareaComment.style.resize = "none";
    textareaComment.style.boxSizing = "border-box";

    const sendButton = document.createElement("button");

    sendButton.onclick = function () {
        console.log("click");
        sendData();
        fakeContainer.removeChild(container);
    };

    sendButton.id = "sendFakeComment";
    sendButton.textContent = languages[lang].button;
    sendButton.style.marginTop = "10px";
    sendButton.style.display = "flex";
    sendButton.style.flexDirection = "column";
    sendButton.style.alignItems = "center";
    sendButton.style.padding = "12px 18px";
    sendButton.style.width = "50%";
    sendButton.style.margin = "10px auto";
    sendButton.style.borderRadius = "6px";
    sendButton.style.border = "none";
    sendButton.style.color = "#fff";
    sendButton.style.background =
        "linear-gradient(180deg, #4B91F7 0%, #367AF6 100%)";
    sendButton.style.boxShadow =
        "0px 0.5px 1.5px rgba(54, 122, 246, 0.25), inset 0px 0.8px 0px -0.25px rgba(255, 255, 255, 0.2)";
    sendButton.style.userSelect = "none";
    sendButton.style.touchAction = "manipulation";

    container.appendChild(inputName);
    container.appendChild(textareaComment);
    container.appendChild(sendButton);

    const fakeContainer = document.getElementById("fake-container");

    fakeContainer.appendChild(container);

    async function sendData() {
        const name = document.getElementsByClassName(
            "fake-comment-poster-name"
        )[0].value;
        const text = document.getElementsByClassName(
            "fake-comment-poster-text"
        )[0].value;

        console.log(name);
        console.log(text);
        const data = {
            name: name,
            text: text,
            landingId: landingId,
        };

        fetch(`${window.location.origin}/lander/postcomments1/comments.php`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        });
    }
}

main();
